//Ask the user for a string and print out whether this string is a palindrome or not.

import 'dart:io';

void main()
{
  stdout.write("Enter a Word = ");
  String? a = stdin.readLineSync();
  String reverse = a!.split( '').reversed.join();
  if(a==reverse)
{
  print("The given word is a Palindrome");
}
else{
  print("The given word is not a palindrome");
}
}
